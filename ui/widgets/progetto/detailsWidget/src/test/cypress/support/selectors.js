const customElementName = 'progetto-details';
const detailsTitle = '[data-testid=details_title]';
const entityIdCell = '[data-testid=progettoIdValue]';

export {
  customElementName,
  detailsTitle,
  entityIdCell
}
