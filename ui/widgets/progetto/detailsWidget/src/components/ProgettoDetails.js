import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';

import progettoType from 'components/__types__/progetto';
import ProgettoFieldTable from 'components/progetto-field-table/ProgettoFieldTable';

const ProgettoDetails = ({ t, progetto }) => {
  return (
    <Box>
      <h3 data-testid="details_title">
        {t('common.widgetName', {
          widgetNamePlaceholder: 'Progetto',
        })}
      </h3>
      <ProgettoFieldTable progetto={progetto} />
    </Box>
  );
};

ProgettoDetails.propTypes = {
  progetto: progettoType,
  t: PropTypes.func.isRequired,
};

ProgettoDetails.defaultProps = {
  progetto: {},
};

export default withTranslation()(ProgettoDetails);
