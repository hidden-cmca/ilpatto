import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import progettoType from 'components/__types__/progetto';

const ProgettoFieldTable = ({ t, i18n: { language }, progetto }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>{t('common.name')}</TableCell>
        <TableCell>{t('common.value')}</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.id')}</span>
        </TableCell>
        <TableCell data-testid="progettoIdValue">
          <span>{progetto.id}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.titolo')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.titolo}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.descrizione')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.descrizione}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.importo')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.importo}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.dataInizio')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.dataInizio && new Date(progetto.dataInizio).toLocaleDateString(language)}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.dataFine')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.dataFine && new Date(progetto.dataFine).toLocaleDateString(language)}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.stato')}</span>
        </TableCell>
        <TableCell>
          <span>{progetto.stato}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.progetto.attivo')}</span>
        </TableCell>
        <TableCell>
          <span><Checkbox disabled checked={progetto.attivo} /></span>
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

ProgettoFieldTable.propTypes = {
  progetto: progettoType,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({
    language: PropTypes.string,
  }).isRequired,
};

ProgettoFieldTable.defaultProps = {
  progetto: [],
};

export default withTranslation()(ProgettoFieldTable);
