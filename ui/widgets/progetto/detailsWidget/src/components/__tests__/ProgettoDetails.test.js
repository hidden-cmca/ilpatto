import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import 'components/__mocks__/i18n';
import ProgettoDetails from 'components/ProgettoDetails';
import progettoMock from 'components/__mocks__/progettoMocks';

describe('ProgettoDetails component', () => {
  test('renders data in details widget', () => {
    const { getByText } = render(<ProgettoDetails progetto={progettoMock} />);
    
      expect(getByText('entities.progetto.titolo')).toBeInTheDocument();
  });
});
