import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'components/__mocks__/i18n';
import { apiProgettoGet } from 'api/progetto';
import progettoApiGetResponseMock from 'components/__mocks__/progettoMocks';
import ProgettoDetailsContainer from 'components/ProgettoDetailsContainer';

jest.mock('api/progetto');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

beforeEach(() => {
  apiProgettoGet.mockClear();
});

describe('ProgettoDetailsContainer component', () => {
  test('requests data when component is mounted', async () => {
    apiProgettoGet.mockImplementation(() => Promise.resolve(progettoApiGetResponseMock));

    render(<ProgettoDetailsContainer id="1" />);

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
    });
  });

  test('data is shown after mount API call', async () => {
    apiProgettoGet.mockImplementation(() => Promise.resolve(progettoApiGetResponseMock));

    const { getByText } = render(<ProgettoDetailsContainer id="1" />);

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
      expect(getByText('entities.progetto.titolo')).toBeInTheDocument();
      expect(getByText('entities.progetto.descrizione')).toBeInTheDocument();
      expect(getByText('entities.progetto.importo')).toBeInTheDocument();
      expect(getByText('entities.progetto.dataInizio')).toBeInTheDocument();
      expect(getByText('entities.progetto.dataFine')).toBeInTheDocument();
      expect(getByText('entities.progetto.stato')).toBeInTheDocument();
      expect(getByText('entities.progetto.attivo')).toBeInTheDocument();
    });
  });

  test('error is shown after failed API call', async () => {
    const onErrorMock = jest.fn();
    apiProgettoGet.mockImplementation(() => Promise.reject());

    const { getByText } = render(<ProgettoDetailsContainer id="1" onError={onErrorMock} />);

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText('error.dataLoading')).toBeInTheDocument();
    });
  });
});
