const progettoApiGetResponse = {
  titolo: 'Consequatur pariatur id voluptates. Nihil molestias qui veniam illum exercitationem facere non aliquam. Minima error fuga omnis deleniti voluptates dolorum. Aut nihil eligendi ipsam. Distinctio omnis eveniet fuga sed. Deserunt dolorem eligendi et autem.',
  descrizione: 'Iusto aut reprehenderit. Enim dignissimos et ea explicabo rerum rerum sed qui. Ut et quo repudiandae autem. Quaerat veritatis non ut deleniti quod soluta placeat. Impedit cupiditate et ab laudantium. Suscipit eius quidem et eaque.',
  importo: -595.5753335729241,
  dataInizio: '1985-01-17',
  dataFine: '2006-03-19',
  stato: 'ESECUZIONE',
  attivo: true,
};

export default progettoApiGetResponse;
