export const READ_ALL = 'progetto-table/readAll';
export const ERROR_FETCH = 'progetto-table/error';
export const CLEAR_ERRORS = 'progetto-table/clearErrors';
export const CLEAR_ITEMS = 'progetto-table/clearItems';
export const CREATE = 'progetto-table/create';
export const UPDATE = 'progetto-table/update';
export const DELETE = 'progetto-table/delete';
