export const ADD_FILTER = 'progetto-filter/addFilter';
export const UPDATE_FILTER = 'progetto-filter/updateFilter';
export const DELETE_FILTER = 'progetto-filter/deleteFilter';
export const CLEAR_FILTERS = 'progetto-filter/clearFilters';
