export const INPUT_EVENT_TYPES = {
  formUpdate: 'progetto.form.update',
  formCreate: 'progetto.form.create',
  formDelete: 'progetto.form.delete',
};

export const OUTPUT_EVENT_TYPES = {
  select: 'progetto.table.select',
  add: 'progetto.table.add',
  error: 'progetto.table.error',
  delete: 'progetto.table.delete',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
