import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import progettoMocks from 'components/__mocks__/progettoMocks';
import { apiProgettosGet } from 'api/progettos';
import 'i18n/__mocks__/i18nMock';
import ProgettoTableContainer from 'components/ProgettoTableContainer';

jest.mock('api/progettos');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

jest.mock('components/pagination/withPagination', () => {
  const withPagination = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        pagination={{
          onChangeItemsPerPage: () => {},
          onChangeCurrentPage: () => {},
        }}
      />
    );
  };

  return withPagination;
});

describe('ProgettoTableContainer', () => {
  const errorMessageKey = 'error.dataLoading';

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('calls API', async () => {
    apiProgettosGet.mockImplementation(() =>
      Promise.resolve({ progettos: progettoMocks, count: 2 })
    );
    const { queryByText } = render(<ProgettoTableContainer />);

    await wait(() => {
      expect(apiProgettosGet).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  });

  it('shows an error if the API call is not successful', async () => {
    apiProgettosGet.mockImplementation(() => {
      throw new Error();
    });
    const { getByText } = render(<ProgettoTableContainer />);

    wait(() => {
      expect(apiProgettosGet).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  });
});
