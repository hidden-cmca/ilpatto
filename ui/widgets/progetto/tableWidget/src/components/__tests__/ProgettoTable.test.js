import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import i18n from 'components/__mocks__/i18n';
import progettoMocks from 'components/__mocks__/progettoMocks';
import ProgettoTable from 'components/ProgettoTable';

describe('ProgettoTable', () => {
  it('shows progettos', () => {
    const { getByText } = render(<ProgettoTable items={progettoMocks} />);

    expect(getByText(
        progettoMocks[0].titolo
    )).toBeInTheDocument();
    expect(getByText(
        progettoMocks[1].titolo
    )).toBeInTheDocument();

    expect(getByText(
        progettoMocks[0].descrizione
    )).toBeInTheDocument();
    expect(getByText(
        progettoMocks[1].descrizione
    )).toBeInTheDocument();

    expect(getByText(
          progettoMocks[0].importo.toString()
    )).toBeInTheDocument();
    expect(getByText(
          progettoMocks[1].importo.toString()
    )).toBeInTheDocument();

    expect(getByText(
        new Date(progettoMocks[0].dataInizio).toLocaleDateString(i18n.language)
    )).toBeInTheDocument();
    expect(getByText(
        new Date(progettoMocks[1].dataInizio).toLocaleDateString(i18n.language)
    )).toBeInTheDocument();

    expect(getByText(
        new Date(progettoMocks[0].dataFine).toLocaleDateString(i18n.language)
    )).toBeInTheDocument();
    expect(getByText(
        new Date(progettoMocks[1].dataFine).toLocaleDateString(i18n.language)
    )).toBeInTheDocument();

  });

  it('shows no progettos message', () => {
    const { queryByText } = render(<ProgettoTable items={[]} />);

    expect(queryByText(
        progettoMocks[0].titolo
    )).not.toBeInTheDocument();
    expect(queryByText(
        progettoMocks[1].titolo
    )).not.toBeInTheDocument();

    expect(queryByText(
        progettoMocks[0].descrizione
    )).not.toBeInTheDocument();
    expect(queryByText(
        progettoMocks[1].descrizione
    )).not.toBeInTheDocument();

    expect(queryByText(
          progettoMocks[0].importo.toString()
    )).not.toBeInTheDocument();
    expect(queryByText(
          progettoMocks[1].importo.toString()
    )).not.toBeInTheDocument();

    expect(queryByText(
        new Date(progettoMocks[0].dataInizio).toLocaleDateString(i18n.language)
    )).not.toBeInTheDocument();
    expect(queryByText(
        new Date(progettoMocks[1].dataInizio).toLocaleDateString(i18n.language)
    )).not.toBeInTheDocument();

    expect(queryByText(
        new Date(progettoMocks[0].dataFine).toLocaleDateString(i18n.language)
    )).not.toBeInTheDocument();
    expect(queryByText(
        new Date(progettoMocks[1].dataFine).toLocaleDateString(i18n.language)
    )).not.toBeInTheDocument();

    expect(queryByText(
        progettoMocks[0].stato
    )).not.toBeInTheDocument();
    expect(queryByText(
        progettoMocks[1].stato
    )).not.toBeInTheDocument();


    expect(queryByText('entities.progetto.noItems')).toBeInTheDocument();
  });

  it('calls onSelect when the user clicks a table row', () => {
    const onSelectMock = jest.fn();
    const { getByText } = render(
      <ProgettoTable items={progettoMocks} onSelect={onSelectMock} />
    );

    fireEvent.click(getByText(
        progettoMocks[0].titolo
    ));
    expect(onSelectMock).toHaveBeenCalledTimes(1);
  });
});
