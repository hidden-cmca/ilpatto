import PropTypes from 'prop-types';

const progettoType = PropTypes.shape({
  id: PropTypes.number,

  titolo: PropTypes.string,
  descrizione: PropTypes.string,
  importo: PropTypes.number,
  dataInizio: PropTypes.string,
  dataFine: PropTypes.string,
  stato: PropTypes.string,
  attivo: PropTypes.bool,
});

export default progettoType;
