import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import progettoType from 'components/__types__/progetto';

const styles = {
  tableRoot: {
    marginTop: '10px',
  },
  rowRoot: {
    cursor: 'pointer',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  noItems: {
    margin: '15px',
  },
};

const ProgettoTable = ({ items, onSelect, classes, t, i18n, Actions }) => {
  const tableRows = items.map(item => (
    <TableRow hover className={classes.rowRoot} key={item.id} onClick={() => onSelect(item)}>
      <TableCell><span>{item.titolo}</span></TableCell>
      <TableCell><span>{item.descrizione}</span></TableCell>
      <TableCell><span>{item.importo}</span></TableCell>
      <TableCell><span>{new Date(item.dataInizio).toLocaleDateString(i18n.language)}</span></TableCell>
      <TableCell><span>{new Date(item.dataFine).toLocaleDateString(i18n.language)}</span></TableCell>
      <TableCell><span>{item.stato}</span></TableCell>
      <TableCell align="center">
        <Checkbox disabled checked={item.attivo} />
      </TableCell>
      {Actions && (
        <TableCell>
          <Actions item={item} />
        </TableCell>
      )}
    </TableRow>
  ));

  return (items.length ? (
    <Table className={classes.tableRoot} stickyHeader>
      <TableHead>
        <TableRow>
          <TableCell>
            <span>{t('entities.progetto.titolo')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.descrizione')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.importo')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.dataInizio')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.dataFine')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.stato')}</span>
          </TableCell>
          <TableCell>
            <span>{t('entities.progetto.attivo')}</span>
          </TableCell>
          {Actions && <TableCell />}
        </TableRow>
      </TableHead>
      <TableBody>{tableRows}</TableBody>
    </Table>
  ) : (
    <div className={classes.noItems}>{t('entities.progetto.noItems')}</div>
  ));
};

ProgettoTable.propTypes = {
  items: PropTypes.arrayOf(progettoType).isRequired,
  onSelect: PropTypes.func,
  classes: PropTypes.shape({
    rowRoot: PropTypes.string,
    tableRoot: PropTypes.string,
    noItems: PropTypes.string,
  }).isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({ language: PropTypes.string }).isRequired,
  Actions: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
};

ProgettoTable.defaultProps = {
  onSelect: () => {},
  Actions: null,
};

export default withStyles(styles)(withTranslation()(ProgettoTable));
