import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/progettos';

export const apiProgettoGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};

export const apiProgettoPost = async (serviceUrl, progetto) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'POST',
    body: progetto ? JSON.stringify(progetto) : null,
  };
  return request(url, options);
};

export const apiProgettoPut = async (serviceUrl, progetto) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'PUT',
    body: progetto ? JSON.stringify(progetto) : null,
  };
  return request(url, options);
};

export const apiProgettoDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};
