export const INPUT_EVENT_TYPES = {
  tableAdd: 'progetto.table.add',
  tableSelect: 'progetto.table.select',
};

export const OUTPUT_EVENT_TYPES = {
  create: 'progetto.form.create',
  update: 'progetto.form.update',
  errorCreate: 'progetto.form.errorCreate',
  errorUpdate: 'progetto.form.errorUpdate',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
