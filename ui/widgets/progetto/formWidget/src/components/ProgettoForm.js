import 'date-fns';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { formValues, formTouched, formErrors } from 'components/__types__/progetto';
import { withFormik } from 'formik';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'recompose';
import * as Yup from 'yup';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import dateFnsLocales from 'i18n/dateFnsLocales';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ConfirmationDialogTrigger from 'components/common/ConfirmationDialogTrigger';

const styles = theme => ({
  root: {
    margin: theme.spacing(3),
  },
  textField: {
    width: '100%',
  },
});
class ProgettoForm extends PureComponent {
  constructor(props) {
    super(props);
    this.handleConfirmationDialogAction = this.handleConfirmationDialogAction.bind(this);
  }

  handleConfirmationDialogAction(action) {
    const { onDelete, values } = this.props;
    switch (action) {
      case ConfirmationDialogTrigger.CONFIRM: {
        onDelete(values);
        break;
      }
      default:
        break;
    }
  }

  render() {
    const {
      classes,
      values,
      touched,
      errors,
      handleChange,
      handleBlur,
      handleSubmit: formikHandleSubmit,
      onDelete,
      onCancelEditing,
      isSubmitting,
      setFieldValue,
      t,
      i18n,
    } = this.props;

    const handleDateChange = field => value => {
      setFieldValue(field, value);
    };

    const dateLabelFn = date => (date ? new Date(date).toLocaleDateString(i18n.language) : '');
    const getHelperText = field => (errors[field] && touched[field] ? errors[field] : '');


    const handleSubmit = e => {
      e.stopPropagation(); // avoids double submission caused by react-shadow-dom-retarget-events
      formikHandleSubmit(e);
    };

    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={dateFnsLocales[i18n.language]}>
        <form onSubmit={handleSubmit} className={classes.root} data-testid="progetto-form">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                id="progetto-titolo"
                error={errors.titolo && touched.titolo}
                helperText={getHelperText('titolo')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.titolo}
                name="titolo"
                label={t('entities.progetto.titolo')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="progetto-descrizione"
                error={errors.descrizione && touched.descrizione}
                helperText={getHelperText('descrizione')}
                className={classes.textField}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.descrizione}
                name="descrizione"
                label={t('entities.progetto.descrizione')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="progetto-importo"
                error={errors.importo && touched.importo}
                helperText={getHelperText('importo')}
                className={classes.textField}
                type="number"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.importo}
                name="importo"
                label={t('entities.progetto.importo')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DatePicker
                id="progetto-dataInizio"
                error={errors.dataInizio && touched.dataInizio}
                helperText={getHelperText('dataInizio')}
                className={classes.textField}
                onChange={handleDateChange('dataInizio')}
                value={values.dataInizio}
                labelFunc={dateLabelFn}
                name="dataInizio"
                label={t('entities.progetto.dataInizio')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DatePicker
                id="progetto-dataFine"
                error={errors.dataFine && touched.dataFine}
                helperText={getHelperText('dataFine')}
                className={classes.textField}
                onChange={handleDateChange('dataFine')}
                value={values.dataFine}
                labelFunc={dateLabelFn}
                name="dataFine"
                label={t('entities.progetto.dataFine')}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel htmlFor="progetto-stato">{t('entities.progetto.stato')}</InputLabel>
              <Select
                native
                id="progetto-stato"
                error={errors.stato && touched.stato}
                className={classes.textField}
                value={values.stato}
                name="stato"
                onChange={handleChange}
              >
                {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
                <option value="" />
                <option value="PROGETTAZIONE">PROGETTAZIONE</option>
                <option value="PROGRAMMAZIONE">PROGRAMMAZIONE</option>
                <option value="ESECUZIONE">ESECUZIONE</option>
              </Select>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControlLabel
                control={
                  // eslint-disable-next-line react/jsx-wrap-multilines
                  <Checkbox
                    id="progetto-attivo"
                    name="attivo"
                    onChange={handleChange}
                    inputProps={{ 'data-testid': 'progetto-attivo-checkbox' }}
                    checked={values.attivo}
                    value="progetto-attivo"
                    color="primary"
                  />
                }
                label={t('entities.progetto.attivo')}
              />
            </Grid>
            {onDelete && (
              <ConfirmationDialogTrigger
                onCloseDialog={this.handleConfirmationDialogAction}
                dialog={{
                  title: t('entities.progetto.deleteDialog.title'),
                  description: t('entities.progetto.deleteDialog.description'),
                  confirmLabel: t('common.yes'),
                  discardLabel: t('common.no'),
                }}
                Renderer={({ onClick }) => (
                  <Button onClick={onClick} disabled={isSubmitting}>
                    {t('common.delete')}
                  </Button>
                )}
              />
            )}

            <Button onClick={onCancelEditing} disabled={isSubmitting} data-testid="cancel-btn">
              {t('common.cancel')}
            </Button>

            <Button type="submit" color="primary" disabled={isSubmitting} data-testid="submit-btn">
              {t('common.save')}
            </Button>
          </Grid>
        </form>
      </MuiPickersUtilsProvider>
    );
  }
}

ProgettoForm.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    textField: PropTypes.string,
    submitButton: PropTypes.string,
    button: PropTypes.string,
    downloadAnchor: PropTypes.string,
  }),
  values: formValues,
  touched: formTouched,
  errors: formErrors,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func,
  onCancelEditing: PropTypes.func,
  isSubmitting: PropTypes.bool.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.shape({ language: PropTypes.string }).isRequired,
};

ProgettoForm.defaultProps = {
  onCancelEditing: () => {},
  classes: {},
  values: {},
  touched: {},
  errors: {},
  onDelete: null,
};

const emptyProgetto = {
  titolo: '',
  descrizione: '',
  importo: '',
  dataInizio: null,
  dataFine: null,
  stato: '',
  attivo: false,
};

const validationSchema = Yup.object().shape({
  titolo: Yup.string(),
  descrizione: Yup.string(),
  importo: Yup.number(),
  dataInizio: Yup.date().nullable(),
  dataFine: Yup.date().nullable(),
  stato: Yup.string(),
  attivo: Yup.boolean(),
});

const formikBag = {
  mapPropsToValues: ({ progetto }) => progetto || emptyProgetto,

  enableReinitialize: true,

  validationSchema,

  handleSubmit: (values, { setSubmitting, props: { onSubmit } }) => {
    onSubmit(values);
    setSubmitting(false);
  },

  displayName: 'ProgettoForm',
};

export default compose(
  withStyles(styles, { withTheme: true }),
  withTranslation(),
  withFormik(formikBag)
)(ProgettoForm);
