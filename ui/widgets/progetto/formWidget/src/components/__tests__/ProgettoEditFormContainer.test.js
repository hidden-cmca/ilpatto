import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiProgettoGet, apiProgettoPut } from 'api/progettos';
import ProgettoEditFormContainer from 'components/ProgettoEditFormContainer';
import 'i18n/__mocks__/i18nMock';
import progettoMock from 'components/__mocks__/progettoMocks';

jest.mock('api/progettos');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('ProgettoEditFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onUpdateMock = jest.fn();

  it('loads data', async () => {
    apiProgettoGet.mockImplementation(() => Promise.resolve(progettoMock));
    const { queryByText } = render(
      <ProgettoEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
      expect(apiProgettoGet).toHaveBeenCalledWith('', '1');
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
    });
  }, 7000);

  it('saves data', async () => {
    apiProgettoGet.mockImplementation(() => Promise.resolve(progettoMock));
    apiProgettoPut.mockImplementation(() => Promise.resolve(progettoMock));

    const { findByTestId, queryByText } = render(
      <ProgettoEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiProgettoPut).toHaveBeenCalledTimes(1);
      expect(apiProgettoPut).toHaveBeenCalledWith('', progettoMock);
      expect(queryByText(successMessageKey)).toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully loaded', async () => {
    apiProgettoGet.mockImplementation(() => Promise.reject());
    const { queryByText } = render(
      <ProgettoEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
      expect(apiProgettoGet).toHaveBeenCalledWith('', '1');
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
      expect(queryByText(successMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiProgettoGet.mockImplementation(() => Promise.resolve(progettoMock));
    apiProgettoPut.mockImplementation(() => Promise.reject());
    const { findByTestId, getByText } = render(
      <ProgettoEditFormContainer id="1" onError={onErrorMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiProgettoGet).toHaveBeenCalledTimes(1);
      expect(apiProgettoGet).toHaveBeenCalledWith('', '1');

      expect(apiProgettoPut).toHaveBeenCalledTimes(1);
      expect(apiProgettoPut).toHaveBeenCalledWith('', progettoMock);

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
