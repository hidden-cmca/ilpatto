import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, wait } from '@testing-library/react';
import i18n from 'i18n/__mocks__/i18nMock';
import progettoMock from 'components/__mocks__/progettoMocks';
import ProgettoForm from 'components/ProgettoForm';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme();

describe('Progetto Form', () => {
  it('shows form', () => {
    const { getByLabelText } = render(
      <ThemeProvider theme={theme}>
        <ProgettoForm progetto={progettoMock} />
      </ThemeProvider>
    );

    expect(getByLabelText('entities.progetto.titolo').value).toBe(
        progettoMock.titolo
    );
    expect(getByLabelText('entities.progetto.descrizione').value).toBe(
        progettoMock.descrizione
    );
    expect(getByLabelText('entities.progetto.importo').value).toBe(
        progettoMock.importo.toString()
    );
    expect(getByLabelText('entities.progetto.dataInizio').value).toBe(
        new Date(progettoMock.dataInizio).toLocaleDateString(i18n.language)
    );
    expect(getByLabelText('entities.progetto.dataFine').value).toBe(
        new Date(progettoMock.dataFine).toLocaleDateString(i18n.language)
    );
    expect(getByLabelText('entities.progetto.stato').value).toBe(
        progettoMock.stato
    );
    expect(getByLabelText('entities.progetto.attivo').value).toBe(
        'progetto-active'
    );
  });

  it('submits form', async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <ProgettoForm progetto={progettoMock} onSubmit={handleSubmit} />
      </ThemeProvider>
    );

    const form = getByTestId('progetto-form');
    fireEvent.submit(form);

    await wait(() => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
  });
});
