import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiProgettoPost } from 'api/progettos';
import ProgettoAddFormContainer from 'components/ProgettoAddFormContainer';
import 'i18n/__mocks__/i18nMock';
import progettoMock from 'components/__mocks__/progettoMocks';

jest.mock('api/progettos');
jest.mock('@material-ui/pickers', () => {
  // eslint-disable-next-line react/prop-types
  const MockPicker = ({ id, value, name, label, onChange }) => {
    const handleChange = event => onChange(event.currentTarget.value);
    return (
      <span>
        <label htmlFor={id}>{label}</label>
        <input id={id} name={name} value={value || ''} onChange={handleChange} />
      </span>
    );
  };
  return {
    ...jest.requireActual('@material-ui/pickers'),
    DateTimePicker: MockPicker,
    DatePicker: MockPicker,
  };
});

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('ProgettoAddFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onCreateMock = jest.fn();

  it('saves data', async () => {
    apiProgettoPost.mockImplementation(data => Promise.resolve(data));

    const { findByTestId, findByLabelText, queryByText, rerender } = render(
      <ProgettoAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />
    );

    const titoloField = await findByLabelText('entities.progetto.titolo');
    fireEvent.change(titoloField, { target: { value: progettoMock.titolo } });
    const descrizioneField = await findByLabelText('entities.progetto.descrizione');
    fireEvent.change(descrizioneField, { target: { value: progettoMock.descrizione } });
    const importoField = await findByLabelText('entities.progetto.importo');
    fireEvent.change(importoField, { target: { value: progettoMock.importo } });
    const dataInizioField = await findByLabelText('entities.progetto.dataInizio');
    fireEvent.change(dataInizioField, { target: { value: progettoMock.dataInizio } });
    const dataFineField = await findByLabelText('entities.progetto.dataFine');
    fireEvent.change(dataFineField, { target: { value: progettoMock.dataFine } });
    const statoField = await findByLabelText('entities.progetto.stato');
    fireEvent.change(statoField, { target: { value: progettoMock.stato } });
    if (progettoMock.attivo) {
      const attivoField = await findByTestId('progetto-attivo-checkbox');
      fireEvent.click(attivoField);
    }
    rerender(<ProgettoAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />);

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiProgettoPost).toHaveBeenCalledTimes(1);
      expect(apiProgettoPost).toHaveBeenCalledWith('', progettoMock);

      expect(queryByText(successMessageKey)).toBeInTheDocument();

      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiProgettoPost.mockImplementation(() => Promise.reject());

    const { findByTestId, findByLabelText, queryByText, rerender } = render(
      <ProgettoAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />
    );

    const titoloField = await findByLabelText('entities.progetto.titolo');
    fireEvent.change(titoloField, { target: { value: progettoMock.titolo } });
    const descrizioneField = await findByLabelText('entities.progetto.descrizione');
    fireEvent.change(descrizioneField, { target: { value: progettoMock.descrizione } });
    const importoField = await findByLabelText('entities.progetto.importo');
    fireEvent.change(importoField, { target: { value: progettoMock.importo } });
    const dataInizioField = await findByLabelText('entities.progetto.dataInizio');
    fireEvent.change(dataInizioField, { target: { value: progettoMock.dataInizio } });
    const dataFineField = await findByLabelText('entities.progetto.dataFine');
    fireEvent.change(dataFineField, { target: { value: progettoMock.dataFine } });
    const statoField = await findByLabelText('entities.progetto.stato');
    fireEvent.change(statoField, { target: { value: progettoMock.stato } });
    if (progettoMock.attivo) {
      const attivoField = await findByTestId('progetto-attivo-checkbox');
      fireEvent.click(attivoField);
    }
    rerender(<ProgettoAddFormContainer onError={onErrorMock} onUpdate={onCreateMock} />);

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiProgettoPost).toHaveBeenCalledTimes(1);
      expect(apiProgettoPost).toHaveBeenCalledWith('', progettoMock);

      expect(queryByText(successMessageKey)).not.toBeInTheDocument();

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
