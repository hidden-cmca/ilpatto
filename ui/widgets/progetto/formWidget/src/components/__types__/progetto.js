import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,

  titolo: PropTypes.string,
  descrizione: PropTypes.string,
  importo: PropTypes.number,
  dataInizio: PropTypes.string,
  dataFine: PropTypes.string,
  stato: PropTypes.string,
  attivo: PropTypes.bool,
});

export const formValues = PropTypes.shape({
  titolo: PropTypes.string,
  descrizione: PropTypes.string,
  importo: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  dataInizio: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  dataFine: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
  stato: PropTypes.string,
  attivo: PropTypes.bool,
});

export const formTouched = PropTypes.shape({
  titolo: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  descrizione: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  importo: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  dataInizio: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  dataFine: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  stato: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
  attivo: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
});

export const formErrors = PropTypes.shape({
  titolo: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  descrizione: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  importo: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  dataInizio: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  dataFine: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  stato: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
  attivo: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
});
