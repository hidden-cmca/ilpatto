import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/settore-prioritarios';

export const apiSettorePrioritarioGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};

export const apiSettorePrioritarioPost = async (serviceUrl, settorePrioritario) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'POST',
    body: settorePrioritario ? JSON.stringify(settorePrioritario) : null,
  };
  return request(url, options);
};

export const apiSettorePrioritarioPut = async (serviceUrl, settorePrioritario) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'PUT',
    body: settorePrioritario ? JSON.stringify(settorePrioritario) : null,
  };
  return request(url, options);
};

export const apiSettorePrioritarioDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};
