import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiSettorePrioritarioGet, apiSettorePrioritarioPut } from 'api/settorePrioritarios';
import SettorePrioritarioEditFormContainer from 'components/SettorePrioritarioEditFormContainer';
import 'i18n/__mocks__/i18nMock';
import settorePrioritarioMock from 'components/__mocks__/settorePrioritarioMocks';

jest.mock('api/settorePrioritarios');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('SettorePrioritarioEditFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onUpdateMock = jest.fn();

  it('loads data', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.resolve(settorePrioritarioMock));
    const { queryByText } = render(
      <SettorePrioritarioEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
      expect(apiSettorePrioritarioGet).toHaveBeenCalledWith('', '1');
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
    });
  }, 7000);

  it('saves data', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.resolve(settorePrioritarioMock));
    apiSettorePrioritarioPut.mockImplementation(() => Promise.resolve(settorePrioritarioMock));

    const { findByTestId, queryByText } = render(
      <SettorePrioritarioEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiSettorePrioritarioPut).toHaveBeenCalledTimes(1);
      expect(apiSettorePrioritarioPut).toHaveBeenCalledWith('', settorePrioritarioMock);
      expect(queryByText(successMessageKey)).toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully loaded', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.reject());
    const { queryByText } = render(
      <SettorePrioritarioEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
      expect(apiSettorePrioritarioGet).toHaveBeenCalledWith('', '1');
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
      expect(queryByText(successMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.resolve(settorePrioritarioMock));
    apiSettorePrioritarioPut.mockImplementation(() => Promise.reject());
    const { findByTestId, getByText } = render(
      <SettorePrioritarioEditFormContainer id="1" onError={onErrorMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
      expect(apiSettorePrioritarioGet).toHaveBeenCalledWith('', '1');

      expect(apiSettorePrioritarioPut).toHaveBeenCalledTimes(1);
      expect(apiSettorePrioritarioPut).toHaveBeenCalledWith('', settorePrioritarioMock);

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
