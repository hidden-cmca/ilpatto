import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, wait } from '@testing-library/react';
import i18n from 'i18n/__mocks__/i18nMock';
import settorePrioritarioMock from 'components/__mocks__/settorePrioritarioMocks';
import SettorePrioritarioForm from 'components/SettorePrioritarioForm';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme();

describe('SettorePrioritario Form', () => {
  it('shows form', () => {
    const { getByLabelText } = render(
      <ThemeProvider theme={theme}>
        <SettorePrioritarioForm settorePrioritario={settorePrioritarioMock} />
      </ThemeProvider>
    );

    expect(getByLabelText('entities.settorePrioritario.titolo').value).toBe(
        settorePrioritarioMock.titolo
    );
  });

  it('submits form', async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <SettorePrioritarioForm settorePrioritario={settorePrioritarioMock} onSubmit={handleSubmit} />
      </ThemeProvider>
    );

    const form = getByTestId('settorePrioritario-form');
    fireEvent.submit(form);

    await wait(() => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
  });
});
