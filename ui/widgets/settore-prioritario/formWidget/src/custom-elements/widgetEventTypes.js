export const INPUT_EVENT_TYPES = {
  tableAdd: 'settorePrioritario.table.add',
  tableSelect: 'settorePrioritario.table.select',
};

export const OUTPUT_EVENT_TYPES = {
  create: 'settorePrioritario.form.create',
  update: 'settorePrioritario.form.update',
  errorCreate: 'settorePrioritario.form.errorCreate',
  errorUpdate: 'settorePrioritario.form.errorUpdate',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
