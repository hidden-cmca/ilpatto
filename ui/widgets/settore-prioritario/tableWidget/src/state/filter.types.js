export const ADD_FILTER = 'settorePrioritario-filter/addFilter';
export const UPDATE_FILTER = 'settorePrioritario-filter/updateFilter';
export const DELETE_FILTER = 'settorePrioritario-filter/deleteFilter';
export const CLEAR_FILTERS = 'settorePrioritario-filter/clearFilters';
