export const READ_ALL = 'settorePrioritario-table/readAll';
export const ERROR_FETCH = 'settorePrioritario-table/error';
export const CLEAR_ERRORS = 'settorePrioritario-table/clearErrors';
export const CLEAR_ITEMS = 'settorePrioritario-table/clearItems';
export const CREATE = 'settorePrioritario-table/create';
export const UPDATE = 'settorePrioritario-table/update';
export const DELETE = 'settorePrioritario-table/delete';
