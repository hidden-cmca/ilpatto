export const INPUT_EVENT_TYPES = {
  formUpdate: 'settorePrioritario.form.update',
  formCreate: 'settorePrioritario.form.create',
  formDelete: 'settorePrioritario.form.delete',
};

export const OUTPUT_EVENT_TYPES = {
  select: 'settorePrioritario.table.select',
  add: 'settorePrioritario.table.add',
  error: 'settorePrioritario.table.error',
  delete: 'settorePrioritario.table.delete',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
