import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import i18n from 'components/__mocks__/i18n';
import settorePrioritarioMocks from 'components/__mocks__/settorePrioritarioMocks';
import SettorePrioritarioTable from 'components/SettorePrioritarioTable';

describe('SettorePrioritarioTable', () => {
  it('shows settorePrioritarios', () => {
    const { getByText } = render(<SettorePrioritarioTable items={settorePrioritarioMocks} />);

    expect(getByText(
        settorePrioritarioMocks[0].titolo
    )).toBeInTheDocument();
    expect(getByText(
        settorePrioritarioMocks[1].titolo
    )).toBeInTheDocument();

  });

  it('shows no settorePrioritarios message', () => {
    const { queryByText } = render(<SettorePrioritarioTable items={[]} />);

    expect(queryByText(
        settorePrioritarioMocks[0].titolo
    )).not.toBeInTheDocument();
    expect(queryByText(
        settorePrioritarioMocks[1].titolo
    )).not.toBeInTheDocument();


    expect(queryByText('entities.settorePrioritario.noItems')).toBeInTheDocument();
  });

  it('calls onSelect when the user clicks a table row', () => {
    const onSelectMock = jest.fn();
    const { getByText } = render(
      <SettorePrioritarioTable items={settorePrioritarioMocks} onSelect={onSelectMock} />
    );

    fireEvent.click(getByText(
        settorePrioritarioMocks[0].titolo
    ));
    expect(onSelectMock).toHaveBeenCalledTimes(1);
  });
});
