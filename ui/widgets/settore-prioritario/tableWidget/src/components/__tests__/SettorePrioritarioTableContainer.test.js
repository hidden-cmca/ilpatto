import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import settorePrioritarioMocks from 'components/__mocks__/settorePrioritarioMocks';
import { apiSettorePrioritariosGet } from 'api/settorePrioritarios';
import 'i18n/__mocks__/i18nMock';
import SettorePrioritarioTableContainer from 'components/SettorePrioritarioTableContainer';

jest.mock('api/settorePrioritarios');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

jest.mock('components/pagination/withPagination', () => {
  const withPagination = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        pagination={{
          onChangeItemsPerPage: () => {},
          onChangeCurrentPage: () => {},
        }}
      />
    );
  };

  return withPagination;
});

describe('SettorePrioritarioTableContainer', () => {
  const errorMessageKey = 'error.dataLoading';

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('calls API', async () => {
    apiSettorePrioritariosGet.mockImplementation(() =>
      Promise.resolve({ settorePrioritarios: settorePrioritarioMocks, count: 2 })
    );
    const { queryByText } = render(<SettorePrioritarioTableContainer />);

    await wait(() => {
      expect(apiSettorePrioritariosGet).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  });

  it('shows an error if the API call is not successful', async () => {
    apiSettorePrioritariosGet.mockImplementation(() => {
      throw new Error();
    });
    const { getByText } = render(<SettorePrioritarioTableContainer />);

    wait(() => {
      expect(apiSettorePrioritariosGet).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  });
});
