import PropTypes from 'prop-types';

const settorePrioritarioType = PropTypes.shape({
  id: PropTypes.number,

  titolo: PropTypes.string,
});

export default settorePrioritarioType;
