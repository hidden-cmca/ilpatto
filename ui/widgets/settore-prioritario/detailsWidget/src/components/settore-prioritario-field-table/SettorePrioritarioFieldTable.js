import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import settorePrioritarioType from 'components/__types__/settorePrioritario';

const SettorePrioritarioFieldTable = ({ t, settorePrioritario }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>{t('common.name')}</TableCell>
        <TableCell>{t('common.value')}</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>
          <span>{t('entities.settorePrioritario.id')}</span>
        </TableCell>
        <TableCell data-testid="settorePrioritarioIdValue">
          <span>{settorePrioritario.id}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.settorePrioritario.titolo')}</span>
        </TableCell>
        <TableCell>
          <span>{settorePrioritario.titolo}</span>
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

SettorePrioritarioFieldTable.propTypes = {
  settorePrioritario: settorePrioritarioType,
  t: PropTypes.func.isRequired,
};

SettorePrioritarioFieldTable.defaultProps = {
  settorePrioritario: [],
};

export default withTranslation()(SettorePrioritarioFieldTable);
