import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'components/__mocks__/i18n';
import { apiSettorePrioritarioGet } from 'api/settorePrioritario';
import settorePrioritarioApiGetResponseMock from 'components/__mocks__/settorePrioritarioMocks';
import SettorePrioritarioDetailsContainer from 'components/SettorePrioritarioDetailsContainer';

jest.mock('api/settorePrioritario');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

beforeEach(() => {
  apiSettorePrioritarioGet.mockClear();
});

describe('SettorePrioritarioDetailsContainer component', () => {
  test('requests data when component is mounted', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.resolve(settorePrioritarioApiGetResponseMock));

    render(<SettorePrioritarioDetailsContainer id="1" />);

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
    });
  });

  test('data is shown after mount API call', async () => {
    apiSettorePrioritarioGet.mockImplementation(() => Promise.resolve(settorePrioritarioApiGetResponseMock));

    const { getByText } = render(<SettorePrioritarioDetailsContainer id="1" />);

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
      expect(getByText('entities.settorePrioritario.titolo')).toBeInTheDocument();
    });
  });

  test('error is shown after failed API call', async () => {
    const onErrorMock = jest.fn();
    apiSettorePrioritarioGet.mockImplementation(() => Promise.reject());

    const { getByText } = render(<SettorePrioritarioDetailsContainer id="1" onError={onErrorMock} />);

    await wait(() => {
      expect(apiSettorePrioritarioGet).toHaveBeenCalledTimes(1);
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText('error.dataLoading')).toBeInTheDocument();
    });
  });
});
