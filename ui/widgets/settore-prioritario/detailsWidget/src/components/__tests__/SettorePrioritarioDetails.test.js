import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import 'components/__mocks__/i18n';
import SettorePrioritarioDetails from 'components/SettorePrioritarioDetails';
import settorePrioritarioMock from 'components/__mocks__/settorePrioritarioMocks';

describe('SettorePrioritarioDetails component', () => {
  test('renders data in details widget', () => {
    const { getByText } = render(<SettorePrioritarioDetails settorePrioritario={settorePrioritarioMock} />);
    
      expect(getByText('entities.settorePrioritario.titolo')).toBeInTheDocument();
  });
});
