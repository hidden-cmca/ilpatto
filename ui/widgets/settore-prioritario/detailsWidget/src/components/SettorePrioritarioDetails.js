import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';

import settorePrioritarioType from 'components/__types__/settorePrioritario';
import SettorePrioritarioFieldTable from 'components/settore-prioritario-field-table/SettorePrioritarioFieldTable';

const SettorePrioritarioDetails = ({ t, settorePrioritario }) => {
  return (
    <Box>
      <h3 data-testid="details_title">
        {t('common.widgetName', {
          widgetNamePlaceholder: 'Settore Prioritario',
        })}
      </h3>
      <SettorePrioritarioFieldTable settorePrioritario={settorePrioritario} />
    </Box>
  );
};

SettorePrioritarioDetails.propTypes = {
  settorePrioritario: settorePrioritarioType,
  t: PropTypes.func.isRequired,
};

SettorePrioritarioDetails.defaultProps = {
  settorePrioritario: {},
};

export default withTranslation()(SettorePrioritarioDetails);
