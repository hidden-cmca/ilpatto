const customElementName = 'settore-prioritario-details';
const detailsTitle = '[data-testid=details_title]';
const entityIdCell = '[data-testid=settorePrioritarioIdValue]';

export {
  customElementName,
  detailsTitle,
  entityIdCell
}
