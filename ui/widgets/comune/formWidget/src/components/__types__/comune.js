import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,

  nome: PropTypes.string,
});

export const formValues = PropTypes.shape({
  nome: PropTypes.string,
});

export const formTouched = PropTypes.shape({
  nome: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
});

export const formErrors = PropTypes.shape({
  nome: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
});
