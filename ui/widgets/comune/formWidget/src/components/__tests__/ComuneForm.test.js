import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, wait } from '@testing-library/react';
import i18n from 'i18n/__mocks__/i18nMock';
import comuneMock from 'components/__mocks__/comuneMocks';
import ComuneForm from 'components/ComuneForm';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme();

describe('Comune Form', () => {
  it('shows form', () => {
    const { getByLabelText } = render(
      <ThemeProvider theme={theme}>
        <ComuneForm comune={comuneMock} />
      </ThemeProvider>
    );

    expect(getByLabelText('entities.comune.nome').value).toBe(
        comuneMock.nome
    );
  });

  it('submits form', async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <ComuneForm comune={comuneMock} onSubmit={handleSubmit} />
      </ThemeProvider>
    );

    const form = getByTestId('comune-form');
    fireEvent.submit(form);

    await wait(() => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
  });
});
