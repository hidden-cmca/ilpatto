import React from 'react';
import { fireEvent, render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { apiComuneGet, apiComunePut } from 'api/comunes';
import ComuneEditFormContainer from 'components/ComuneEditFormContainer';
import 'i18n/__mocks__/i18nMock';
import comuneMock from 'components/__mocks__/comuneMocks';

jest.mock('api/comunes');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

describe('ComuneEditFormContainer', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const errorMessageKey = 'error.dataLoading';
  const successMessageKey = 'common.dataSaved';

  const onErrorMock = jest.fn();
  const onUpdateMock = jest.fn();

  it('loads data', async () => {
    apiComuneGet.mockImplementation(() => Promise.resolve(comuneMock));
    const { queryByText } = render(
      <ComuneEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
      expect(apiComuneGet).toHaveBeenCalledWith('', '1');
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
    });
  }, 7000);

  it('saves data', async () => {
    apiComuneGet.mockImplementation(() => Promise.resolve(comuneMock));
    apiComunePut.mockImplementation(() => Promise.resolve(comuneMock));

    const { findByTestId, queryByText } = render(
      <ComuneEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiComunePut).toHaveBeenCalledTimes(1);
      expect(apiComunePut).toHaveBeenCalledWith('', comuneMock);
      expect(queryByText(successMessageKey)).toBeInTheDocument();
      expect(onErrorMock).toHaveBeenCalledTimes(0);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully loaded', async () => {
    apiComuneGet.mockImplementation(() => Promise.reject());
    const { queryByText } = render(
      <ComuneEditFormContainer id="1" onError={onErrorMock} onUpdate={onUpdateMock} />
    );

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
      expect(apiComuneGet).toHaveBeenCalledWith('', '1');
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).toBeInTheDocument();
      expect(queryByText(successMessageKey)).not.toBeInTheDocument();
    });
  }, 7000);

  it('shows an error if data is not successfully saved', async () => {
    apiComuneGet.mockImplementation(() => Promise.resolve(comuneMock));
    apiComunePut.mockImplementation(() => Promise.reject());
    const { findByTestId, getByText } = render(
      <ComuneEditFormContainer id="1" onError={onErrorMock} />
    );

    const saveButton = await findByTestId('submit-btn');

    fireEvent.click(saveButton);

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
      expect(apiComuneGet).toHaveBeenCalledWith('', '1');

      expect(apiComunePut).toHaveBeenCalledTimes(1);
      expect(apiComunePut).toHaveBeenCalledWith('', comuneMock);

      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  }, 7000);
});
