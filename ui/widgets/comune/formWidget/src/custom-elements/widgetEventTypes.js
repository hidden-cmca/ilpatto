export const INPUT_EVENT_TYPES = {
  tableAdd: 'comune.table.add',
  tableSelect: 'comune.table.select',
};

export const OUTPUT_EVENT_TYPES = {
  create: 'comune.form.create',
  update: 'comune.form.update',
  errorCreate: 'comune.form.errorCreate',
  errorUpdate: 'comune.form.errorUpdate',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
