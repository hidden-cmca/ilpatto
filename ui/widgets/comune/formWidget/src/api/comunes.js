import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/comunes';

export const apiComuneGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};

export const apiComunePost = async (serviceUrl, comune) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'POST',
    body: comune ? JSON.stringify(comune) : null,
  };
  return request(url, options);
};

export const apiComunePut = async (serviceUrl, comune) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'PUT',
    body: comune ? JSON.stringify(comune) : null,
  };
  return request(url, options);
};

export const apiComuneDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};
