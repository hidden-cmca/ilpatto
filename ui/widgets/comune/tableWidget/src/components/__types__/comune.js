import PropTypes from 'prop-types';

const comuneType = PropTypes.shape({
  id: PropTypes.number,

  nome: PropTypes.string,
});

export default comuneType;
