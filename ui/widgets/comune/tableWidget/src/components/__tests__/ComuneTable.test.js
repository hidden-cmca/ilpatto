import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import i18n from 'components/__mocks__/i18n';
import comuneMocks from 'components/__mocks__/comuneMocks';
import ComuneTable from 'components/ComuneTable';

describe('ComuneTable', () => {
  it('shows comunes', () => {
    const { getByText } = render(<ComuneTable items={comuneMocks} />);

    expect(getByText(
        comuneMocks[0].nome
    )).toBeInTheDocument();
    expect(getByText(
        comuneMocks[1].nome
    )).toBeInTheDocument();

  });

  it('shows no comunes message', () => {
    const { queryByText } = render(<ComuneTable items={[]} />);

    expect(queryByText(
        comuneMocks[0].nome
    )).not.toBeInTheDocument();
    expect(queryByText(
        comuneMocks[1].nome
    )).not.toBeInTheDocument();


    expect(queryByText('entities.comune.noItems')).toBeInTheDocument();
  });

  it('calls onSelect when the user clicks a table row', () => {
    const onSelectMock = jest.fn();
    const { getByText } = render(
      <ComuneTable items={comuneMocks} onSelect={onSelectMock} />
    );

    fireEvent.click(getByText(
        comuneMocks[0].nome
    ));
    expect(onSelectMock).toHaveBeenCalledTimes(1);
  });
});
