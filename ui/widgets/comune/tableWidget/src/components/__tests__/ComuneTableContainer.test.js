import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import comuneMocks from 'components/__mocks__/comuneMocks';
import { apiComunesGet } from 'api/comunes';
import 'i18n/__mocks__/i18nMock';
import ComuneTableContainer from 'components/ComuneTableContainer';

jest.mock('api/comunes');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

jest.mock('components/pagination/withPagination', () => {
  const withPagination = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        pagination={{
          onChangeItemsPerPage: () => {},
          onChangeCurrentPage: () => {},
        }}
      />
    );
  };

  return withPagination;
});

describe('ComuneTableContainer', () => {
  const errorMessageKey = 'error.dataLoading';

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('calls API', async () => {
    apiComunesGet.mockImplementation(() =>
      Promise.resolve({ comunes: comuneMocks, count: 2 })
    );
    const { queryByText } = render(<ComuneTableContainer />);

    await wait(() => {
      expect(apiComunesGet).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  });

  it('shows an error if the API call is not successful', async () => {
    apiComunesGet.mockImplementation(() => {
      throw new Error();
    });
    const { getByText } = render(<ComuneTableContainer />);

    wait(() => {
      expect(apiComunesGet).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  });
});
