export const ADD_FILTER = 'comune-filter/addFilter';
export const UPDATE_FILTER = 'comune-filter/updateFilter';
export const DELETE_FILTER = 'comune-filter/deleteFilter';
export const CLEAR_FILTERS = 'comune-filter/clearFilters';
