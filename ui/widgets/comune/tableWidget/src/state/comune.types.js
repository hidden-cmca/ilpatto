export const READ_ALL = 'comune-table/readAll';
export const ERROR_FETCH = 'comune-table/error';
export const CLEAR_ERRORS = 'comune-table/clearErrors';
export const CLEAR_ITEMS = 'comune-table/clearItems';
export const CREATE = 'comune-table/create';
export const UPDATE = 'comune-table/update';
export const DELETE = 'comune-table/delete';
