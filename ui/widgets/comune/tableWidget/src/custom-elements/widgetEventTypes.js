export const INPUT_EVENT_TYPES = {
  formUpdate: 'comune.form.update',
  formCreate: 'comune.form.create',
  formDelete: 'comune.form.delete',
};

export const OUTPUT_EVENT_TYPES = {
  select: 'comune.table.select',
  add: 'comune.table.add',
  error: 'comune.table.error',
  delete: 'comune.table.delete',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
