const customElementName = 'comune-details';
const detailsTitle = '[data-testid=details_title]';
const entityIdCell = '[data-testid=comuneIdValue]';

export {
  customElementName,
  detailsTitle,
  entityIdCell
}
