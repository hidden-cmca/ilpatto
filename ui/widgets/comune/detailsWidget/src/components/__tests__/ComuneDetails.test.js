import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import 'components/__mocks__/i18n';
import ComuneDetails from 'components/ComuneDetails';
import comuneMock from 'components/__mocks__/comuneMocks';

describe('ComuneDetails component', () => {
  test('renders data in details widget', () => {
    const { getByText } = render(<ComuneDetails comune={comuneMock} />);
    
      expect(getByText('entities.comune.nome')).toBeInTheDocument();
  });
});
