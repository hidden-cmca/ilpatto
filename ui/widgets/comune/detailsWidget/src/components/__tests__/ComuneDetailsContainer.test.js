import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'components/__mocks__/i18n';
import { apiComuneGet } from 'api/comune';
import comuneApiGetResponseMock from 'components/__mocks__/comuneMocks';
import ComuneDetailsContainer from 'components/ComuneDetailsContainer';

jest.mock('api/comune');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

beforeEach(() => {
  apiComuneGet.mockClear();
});

describe('ComuneDetailsContainer component', () => {
  test('requests data when component is mounted', async () => {
    apiComuneGet.mockImplementation(() => Promise.resolve(comuneApiGetResponseMock));

    render(<ComuneDetailsContainer id="1" />);

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
    });
  });

  test('data is shown after mount API call', async () => {
    apiComuneGet.mockImplementation(() => Promise.resolve(comuneApiGetResponseMock));

    const { getByText } = render(<ComuneDetailsContainer id="1" />);

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
      expect(getByText('entities.comune.nome')).toBeInTheDocument();
    });
  });

  test('error is shown after failed API call', async () => {
    const onErrorMock = jest.fn();
    apiComuneGet.mockImplementation(() => Promise.reject());

    const { getByText } = render(<ComuneDetailsContainer id="1" onError={onErrorMock} />);

    await wait(() => {
      expect(apiComuneGet).toHaveBeenCalledTimes(1);
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText('error.dataLoading')).toBeInTheDocument();
    });
  });
});
