import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import comuneType from 'components/__types__/comune';

const ComuneFieldTable = ({ t, comune }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>{t('common.name')}</TableCell>
        <TableCell>{t('common.value')}</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>
          <span>{t('entities.comune.id')}</span>
        </TableCell>
        <TableCell data-testid="comuneIdValue">
          <span>{comune.id}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.comune.nome')}</span>
        </TableCell>
        <TableCell>
          <span>{comune.nome}</span>
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

ComuneFieldTable.propTypes = {
  comune: comuneType,
  t: PropTypes.func.isRequired,
};

ComuneFieldTable.defaultProps = {
  comune: [],
};

export default withTranslation()(ComuneFieldTable);
