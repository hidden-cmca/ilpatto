import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';

import comuneType from 'components/__types__/comune';
import ComuneFieldTable from 'components/comune-field-table/ComuneFieldTable';

const ComuneDetails = ({ t, comune }) => {
  return (
    <Box>
      <h3 data-testid="details_title">
        {t('common.widgetName', {
          widgetNamePlaceholder: 'Comune',
        })}
      </h3>
      <ComuneFieldTable comune={comune} />
    </Box>
  );
};

ComuneDetails.propTypes = {
  comune: comuneType,
  t: PropTypes.func.isRequired,
};

ComuneDetails.defaultProps = {
  comune: {},
};

export default withTranslation()(ComuneDetails);
