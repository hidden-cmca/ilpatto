export const ADD_FILTER = 'area-filter/addFilter';
export const UPDATE_FILTER = 'area-filter/updateFilter';
export const DELETE_FILTER = 'area-filter/deleteFilter';
export const CLEAR_FILTERS = 'area-filter/clearFilters';
