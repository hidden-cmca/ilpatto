export const READ_ALL = 'area-table/readAll';
export const ERROR_FETCH = 'area-table/error';
export const CLEAR_ERRORS = 'area-table/clearErrors';
export const CLEAR_ITEMS = 'area-table/clearItems';
export const CREATE = 'area-table/create';
export const UPDATE = 'area-table/update';
export const DELETE = 'area-table/delete';
