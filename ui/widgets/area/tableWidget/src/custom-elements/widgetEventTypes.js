export const INPUT_EVENT_TYPES = {
  formUpdate: 'area.form.update',
  formCreate: 'area.form.create',
  formDelete: 'area.form.delete',
};

export const OUTPUT_EVENT_TYPES = {
  select: 'area.table.select',
  add: 'area.table.add',
  error: 'area.table.error',
  delete: 'area.table.delete',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
