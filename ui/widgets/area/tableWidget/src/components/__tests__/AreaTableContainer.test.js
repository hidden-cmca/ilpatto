import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import areaMocks from 'components/__mocks__/areaMocks';
import { apiAreasGet } from 'api/areas';
import 'i18n/__mocks__/i18nMock';
import AreaTableContainer from 'components/AreaTableContainer';

jest.mock('api/areas');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

jest.mock('components/pagination/withPagination', () => {
  const withPagination = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        pagination={{
          onChangeItemsPerPage: () => {},
          onChangeCurrentPage: () => {},
        }}
      />
    );
  };

  return withPagination;
});

describe('AreaTableContainer', () => {
  const errorMessageKey = 'error.dataLoading';

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('calls API', async () => {
    apiAreasGet.mockImplementation(() =>
      Promise.resolve({ areas: areaMocks, count: 2 })
    );
    const { queryByText } = render(<AreaTableContainer />);

    await wait(() => {
      expect(apiAreasGet).toHaveBeenCalledTimes(1);
      expect(queryByText(errorMessageKey)).not.toBeInTheDocument();
    });
  });

  it('shows an error if the API call is not successful', async () => {
    apiAreasGet.mockImplementation(() => {
      throw new Error();
    });
    const { getByText } = render(<AreaTableContainer />);

    wait(() => {
      expect(apiAreasGet).toHaveBeenCalledTimes(1);
      expect(getByText(errorMessageKey)).toBeInTheDocument();
    });
  });
});
