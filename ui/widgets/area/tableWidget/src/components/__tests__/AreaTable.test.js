import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/react';
import i18n from 'components/__mocks__/i18n';
import areaMocks from 'components/__mocks__/areaMocks';
import AreaTable from 'components/AreaTable';

describe('AreaTable', () => {
  it('shows areas', () => {
    const { getByText } = render(<AreaTable items={areaMocks} />);

    expect(getByText(
        areaMocks[0].titolo
    )).toBeInTheDocument();
    expect(getByText(
        areaMocks[1].titolo
    )).toBeInTheDocument();

  });

  it('shows no areas message', () => {
    const { queryByText } = render(<AreaTable items={[]} />);

    expect(queryByText(
        areaMocks[0].titolo
    )).not.toBeInTheDocument();
    expect(queryByText(
        areaMocks[1].titolo
    )).not.toBeInTheDocument();


    expect(queryByText('entities.area.noItems')).toBeInTheDocument();
  });

  it('calls onSelect when the user clicks a table row', () => {
    const onSelectMock = jest.fn();
    const { getByText } = render(
      <AreaTable items={areaMocks} onSelect={onSelectMock} />
    );

    fireEvent.click(getByText(
        areaMocks[0].titolo
    ));
    expect(onSelectMock).toHaveBeenCalledTimes(1);
  });
});
