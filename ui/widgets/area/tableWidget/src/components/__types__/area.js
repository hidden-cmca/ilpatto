import PropTypes from 'prop-types';

const areaType = PropTypes.shape({
  id: PropTypes.number,

  titolo: PropTypes.string,
});

export default areaType;
