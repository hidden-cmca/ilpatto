const customElementName = 'area-details';
const detailsTitle = '[data-testid=details_title]';
const entityIdCell = '[data-testid=areaIdValue]';

export {
  customElementName,
  detailsTitle,
  entityIdCell
}
