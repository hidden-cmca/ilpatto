import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import areaType from 'components/__types__/area';

const AreaFieldTable = ({ t, area }) => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>{t('common.name')}</TableCell>
        <TableCell>{t('common.value')}</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>
          <span>{t('entities.area.id')}</span>
        </TableCell>
        <TableCell data-testid="areaIdValue">
          <span>{area.id}</span>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <span>{t('entities.area.titolo')}</span>
        </TableCell>
        <TableCell>
          <span>{area.titolo}</span>
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

AreaFieldTable.propTypes = {
  area: areaType,
  t: PropTypes.func.isRequired,
};

AreaFieldTable.defaultProps = {
  area: [],
};

export default withTranslation()(AreaFieldTable);
