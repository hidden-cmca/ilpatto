import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import 'components/__mocks__/i18n';
import AreaDetails from 'components/AreaDetails';
import areaMock from 'components/__mocks__/areaMocks';

describe('AreaDetails component', () => {
  test('renders data in details widget', () => {
    const { getByText } = render(<AreaDetails area={areaMock} />);
    
      expect(getByText('entities.area.titolo')).toBeInTheDocument();
  });
});
