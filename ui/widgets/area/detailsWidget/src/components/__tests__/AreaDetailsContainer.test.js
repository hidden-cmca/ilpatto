import React from 'react';
import { render, wait } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'components/__mocks__/i18n';
import { apiAreaGet } from 'api/area';
import areaApiGetResponseMock from 'components/__mocks__/areaMocks';
import AreaDetailsContainer from 'components/AreaDetailsContainer';

jest.mock('api/area');

jest.mock('auth/withKeycloak', () => {
  const withKeycloak = Component => {
    return props => (
      <Component
        {...props} // eslint-disable-line react/jsx-props-no-spreading
        keycloak={{
          initialized: true,
          authenticated: true,
        }}
      />
    );
  };

  return withKeycloak;
});

beforeEach(() => {
  apiAreaGet.mockClear();
});

describe('AreaDetailsContainer component', () => {
  test('requests data when component is mounted', async () => {
    apiAreaGet.mockImplementation(() => Promise.resolve(areaApiGetResponseMock));

    render(<AreaDetailsContainer id="1" />);

    await wait(() => {
      expect(apiAreaGet).toHaveBeenCalledTimes(1);
    });
  });

  test('data is shown after mount API call', async () => {
    apiAreaGet.mockImplementation(() => Promise.resolve(areaApiGetResponseMock));

    const { getByText } = render(<AreaDetailsContainer id="1" />);

    await wait(() => {
      expect(apiAreaGet).toHaveBeenCalledTimes(1);
      expect(getByText('entities.area.titolo')).toBeInTheDocument();
    });
  });

  test('error is shown after failed API call', async () => {
    const onErrorMock = jest.fn();
    apiAreaGet.mockImplementation(() => Promise.reject());

    const { getByText } = render(<AreaDetailsContainer id="1" onError={onErrorMock} />);

    await wait(() => {
      expect(apiAreaGet).toHaveBeenCalledTimes(1);
      expect(onErrorMock).toHaveBeenCalledTimes(1);
      expect(getByText('error.dataLoading')).toBeInTheDocument();
    });
  });
});
