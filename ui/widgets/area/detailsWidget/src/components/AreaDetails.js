import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import Box from '@material-ui/core/Box';

import areaType from 'components/__types__/area';
import AreaFieldTable from 'components/area-field-table/AreaFieldTable';

const AreaDetails = ({ t, area }) => {
  return (
    <Box>
      <h3 data-testid="details_title">
        {t('common.widgetName', {
          widgetNamePlaceholder: 'Area',
        })}
      </h3>
      <AreaFieldTable area={area} />
    </Box>
  );
};

AreaDetails.propTypes = {
  area: areaType,
  t: PropTypes.func.isRequired,
};

AreaDetails.defaultProps = {
  area: {},
};

export default withTranslation()(AreaDetails);
