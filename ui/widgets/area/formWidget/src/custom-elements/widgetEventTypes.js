export const INPUT_EVENT_TYPES = {
  tableAdd: 'area.table.add',
  tableSelect: 'area.table.select',
};

export const OUTPUT_EVENT_TYPES = {
  create: 'area.form.create',
  update: 'area.form.update',
  errorCreate: 'area.form.errorCreate',
  errorUpdate: 'area.form.errorUpdate',
};

export const KEYCLOAK_EVENT_TYPE = 'keycloak';
