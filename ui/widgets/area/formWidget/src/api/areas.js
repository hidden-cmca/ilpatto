import { getDefaultOptions, request } from 'api/helpers';

const resource = 'api/areas';

export const apiAreaGet = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'GET',
  };
  return request(url, options);
};

export const apiAreaPost = async (serviceUrl, area) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'POST',
    body: area ? JSON.stringify(area) : null,
  };
  return request(url, options);
};

export const apiAreaPut = async (serviceUrl, area) => {
  const url = `${serviceUrl}/${resource}`;
  const options = {
    ...getDefaultOptions(),
    method: 'PUT',
    body: area ? JSON.stringify(area) : null,
  };
  return request(url, options);
};

export const apiAreaDelete = async (serviceUrl, id) => {
  const url = `${serviceUrl}/${resource}/${id}`;
  const options = {
    ...getDefaultOptions(),
    method: 'DELETE',
  };
  return request(url, options);
};
