import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, wait } from '@testing-library/react';
import i18n from 'i18n/__mocks__/i18nMock';
import areaMock from 'components/__mocks__/areaMocks';
import AreaForm from 'components/AreaForm';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme();

describe('Area Form', () => {
  it('shows form', () => {
    const { getByLabelText } = render(
      <ThemeProvider theme={theme}>
        <AreaForm area={areaMock} />
      </ThemeProvider>
    );

    expect(getByLabelText('entities.area.titolo').value).toBe(
        areaMock.titolo
    );
  });

  it('submits form', async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <AreaForm area={areaMock} onSubmit={handleSubmit} />
      </ThemeProvider>
    );

    const form = getByTestId('area-form');
    fireEvent.submit(form);

    await wait(() => {
      expect(handleSubmit).toHaveBeenCalledTimes(1);
    });
  });
});
