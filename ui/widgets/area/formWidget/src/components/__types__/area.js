import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number,

  titolo: PropTypes.string,
});

export const formValues = PropTypes.shape({
  titolo: PropTypes.string,
});

export const formTouched = PropTypes.shape({
  titolo: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape()]),
});

export const formErrors = PropTypes.shape({
  titolo: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
});
