package it.almaviva.ilpatto.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import it.almaviva.ilpatto.web.rest.TestUtil;

public class ProgettoTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Progetto.class);
        Progetto progetto1 = new Progetto();
        progetto1.setId(1L);
        Progetto progetto2 = new Progetto();
        progetto2.setId(progetto1.getId());
        assertThat(progetto1).isEqualTo(progetto2);
        progetto2.setId(2L);
        assertThat(progetto1).isNotEqualTo(progetto2);
        progetto1.setId(null);
        assertThat(progetto1).isNotEqualTo(progetto2);
    }
}
