package it.almaviva.ilpatto.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import it.almaviva.ilpatto.web.rest.TestUtil;

public class SettorePrioritarioTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SettorePrioritario.class);
        SettorePrioritario settorePrioritario1 = new SettorePrioritario();
        settorePrioritario1.setId(1L);
        SettorePrioritario settorePrioritario2 = new SettorePrioritario();
        settorePrioritario2.setId(settorePrioritario1.getId());
        assertThat(settorePrioritario1).isEqualTo(settorePrioritario2);
        settorePrioritario2.setId(2L);
        assertThat(settorePrioritario1).isNotEqualTo(settorePrioritario2);
        settorePrioritario1.setId(null);
        assertThat(settorePrioritario1).isNotEqualTo(settorePrioritario2);
    }
}
