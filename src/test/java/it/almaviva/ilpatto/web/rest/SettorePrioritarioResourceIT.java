package it.almaviva.ilpatto.web.rest;

import it.almaviva.ilpatto.IlpattoApp;
import it.almaviva.ilpatto.config.TestSecurityConfiguration;
import it.almaviva.ilpatto.domain.SettorePrioritario;
import it.almaviva.ilpatto.repository.SettorePrioritarioRepository;
import it.almaviva.ilpatto.service.SettorePrioritarioService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SettorePrioritarioResource} REST controller.
 */
@SpringBootTest(classes = { IlpattoApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class SettorePrioritarioResourceIT {

    private static final String DEFAULT_TITOLO = "AAAAAAAAAA";
    private static final String UPDATED_TITOLO = "BBBBBBBBBB";

    @Autowired
    private SettorePrioritarioRepository settorePrioritarioRepository;

    @Autowired
    private SettorePrioritarioService settorePrioritarioService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSettorePrioritarioMockMvc;

    private SettorePrioritario settorePrioritario;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SettorePrioritario createEntity(EntityManager em) {
        SettorePrioritario settorePrioritario = new SettorePrioritario()
            .titolo(DEFAULT_TITOLO);
        return settorePrioritario;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SettorePrioritario createUpdatedEntity(EntityManager em) {
        SettorePrioritario settorePrioritario = new SettorePrioritario()
            .titolo(UPDATED_TITOLO);
        return settorePrioritario;
    }

    @BeforeEach
    public void initTest() {
        settorePrioritario = createEntity(em);
    }

    @Test
    @Transactional
    public void createSettorePrioritario() throws Exception {
        int databaseSizeBeforeCreate = settorePrioritarioRepository.findAll().size();
        // Create the SettorePrioritario
        restSettorePrioritarioMockMvc.perform(post("/api/settore-prioritarios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(settorePrioritario)))
            .andExpect(status().isCreated());

        // Validate the SettorePrioritario in the database
        List<SettorePrioritario> settorePrioritarioList = settorePrioritarioRepository.findAll();
        assertThat(settorePrioritarioList).hasSize(databaseSizeBeforeCreate + 1);
        SettorePrioritario testSettorePrioritario = settorePrioritarioList.get(settorePrioritarioList.size() - 1);
        assertThat(testSettorePrioritario.getTitolo()).isEqualTo(DEFAULT_TITOLO);
    }

    @Test
    @Transactional
    public void createSettorePrioritarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = settorePrioritarioRepository.findAll().size();

        // Create the SettorePrioritario with an existing ID
        settorePrioritario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSettorePrioritarioMockMvc.perform(post("/api/settore-prioritarios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(settorePrioritario)))
            .andExpect(status().isBadRequest());

        // Validate the SettorePrioritario in the database
        List<SettorePrioritario> settorePrioritarioList = settorePrioritarioRepository.findAll();
        assertThat(settorePrioritarioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSettorePrioritarios() throws Exception {
        // Initialize the database
        settorePrioritarioRepository.saveAndFlush(settorePrioritario);

        // Get all the settorePrioritarioList
        restSettorePrioritarioMockMvc.perform(get("/api/settore-prioritarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(settorePrioritario.getId().intValue())))
            .andExpect(jsonPath("$.[*].titolo").value(hasItem(DEFAULT_TITOLO)));
    }
    
    @Test
    @Transactional
    public void getSettorePrioritario() throws Exception {
        // Initialize the database
        settorePrioritarioRepository.saveAndFlush(settorePrioritario);

        // Get the settorePrioritario
        restSettorePrioritarioMockMvc.perform(get("/api/settore-prioritarios/{id}", settorePrioritario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(settorePrioritario.getId().intValue()))
            .andExpect(jsonPath("$.titolo").value(DEFAULT_TITOLO));
    }
    @Test
    @Transactional
    public void getNonExistingSettorePrioritario() throws Exception {
        // Get the settorePrioritario
        restSettorePrioritarioMockMvc.perform(get("/api/settore-prioritarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSettorePrioritario() throws Exception {
        // Initialize the database
        settorePrioritarioService.save(settorePrioritario);

        int databaseSizeBeforeUpdate = settorePrioritarioRepository.findAll().size();

        // Update the settorePrioritario
        SettorePrioritario updatedSettorePrioritario = settorePrioritarioRepository.findById(settorePrioritario.getId()).get();
        // Disconnect from session so that the updates on updatedSettorePrioritario are not directly saved in db
        em.detach(updatedSettorePrioritario);
        updatedSettorePrioritario
            .titolo(UPDATED_TITOLO);

        restSettorePrioritarioMockMvc.perform(put("/api/settore-prioritarios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSettorePrioritario)))
            .andExpect(status().isOk());

        // Validate the SettorePrioritario in the database
        List<SettorePrioritario> settorePrioritarioList = settorePrioritarioRepository.findAll();
        assertThat(settorePrioritarioList).hasSize(databaseSizeBeforeUpdate);
        SettorePrioritario testSettorePrioritario = settorePrioritarioList.get(settorePrioritarioList.size() - 1);
        assertThat(testSettorePrioritario.getTitolo()).isEqualTo(UPDATED_TITOLO);
    }

    @Test
    @Transactional
    public void updateNonExistingSettorePrioritario() throws Exception {
        int databaseSizeBeforeUpdate = settorePrioritarioRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSettorePrioritarioMockMvc.perform(put("/api/settore-prioritarios").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(settorePrioritario)))
            .andExpect(status().isBadRequest());

        // Validate the SettorePrioritario in the database
        List<SettorePrioritario> settorePrioritarioList = settorePrioritarioRepository.findAll();
        assertThat(settorePrioritarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSettorePrioritario() throws Exception {
        // Initialize the database
        settorePrioritarioService.save(settorePrioritario);

        int databaseSizeBeforeDelete = settorePrioritarioRepository.findAll().size();

        // Delete the settorePrioritario
        restSettorePrioritarioMockMvc.perform(delete("/api/settore-prioritarios/{id}", settorePrioritario.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SettorePrioritario> settorePrioritarioList = settorePrioritarioRepository.findAll();
        assertThat(settorePrioritarioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
