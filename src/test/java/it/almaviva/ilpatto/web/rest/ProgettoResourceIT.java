package it.almaviva.ilpatto.web.rest;

import it.almaviva.ilpatto.IlpattoApp;
import it.almaviva.ilpatto.config.TestSecurityConfiguration;
import it.almaviva.ilpatto.domain.Progetto;
import it.almaviva.ilpatto.repository.ProgettoRepository;
import it.almaviva.ilpatto.service.ProgettoService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.almaviva.ilpatto.domain.enumeration.Stati;
/**
 * Integration tests for the {@link ProgettoResource} REST controller.
 */
@SpringBootTest(classes = { IlpattoApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class ProgettoResourceIT {

    private static final String DEFAULT_TITOLO = "AAAAAAAAAA";
    private static final String UPDATED_TITOLO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final Double DEFAULT_IMPORTO = 1D;
    private static final Double UPDATED_IMPORTO = 2D;

    private static final LocalDate DEFAULT_DATA_INIZIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INIZIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATA_FINE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FINE = LocalDate.now(ZoneId.systemDefault());

    private static final Stati DEFAULT_STATO = Stati.PROGETTAZIONE;
    private static final Stati UPDATED_STATO = Stati.PROGRAMMAZIONE;

    private static final Boolean DEFAULT_ATTIVO = false;
    private static final Boolean UPDATED_ATTIVO = true;

    @Autowired
    private ProgettoRepository progettoRepository;

    @Autowired
    private ProgettoService progettoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProgettoMockMvc;

    private Progetto progetto;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Progetto createEntity(EntityManager em) {
        Progetto progetto = new Progetto()
            .titolo(DEFAULT_TITOLO)
            .descrizione(DEFAULT_DESCRIZIONE)
            .importo(DEFAULT_IMPORTO)
            .dataInizio(DEFAULT_DATA_INIZIO)
            .dataFine(DEFAULT_DATA_FINE)
            .stato(DEFAULT_STATO)
            .attivo(DEFAULT_ATTIVO);
        return progetto;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Progetto createUpdatedEntity(EntityManager em) {
        Progetto progetto = new Progetto()
            .titolo(UPDATED_TITOLO)
            .descrizione(UPDATED_DESCRIZIONE)
            .importo(UPDATED_IMPORTO)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .stato(UPDATED_STATO)
            .attivo(UPDATED_ATTIVO);
        return progetto;
    }

    @BeforeEach
    public void initTest() {
        progetto = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgetto() throws Exception {
        int databaseSizeBeforeCreate = progettoRepository.findAll().size();
        // Create the Progetto
        restProgettoMockMvc.perform(post("/api/progettos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(progetto)))
            .andExpect(status().isCreated());

        // Validate the Progetto in the database
        List<Progetto> progettoList = progettoRepository.findAll();
        assertThat(progettoList).hasSize(databaseSizeBeforeCreate + 1);
        Progetto testProgetto = progettoList.get(progettoList.size() - 1);
        assertThat(testProgetto.getTitolo()).isEqualTo(DEFAULT_TITOLO);
        assertThat(testProgetto.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testProgetto.getImporto()).isEqualTo(DEFAULT_IMPORTO);
        assertThat(testProgetto.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
        assertThat(testProgetto.getDataFine()).isEqualTo(DEFAULT_DATA_FINE);
        assertThat(testProgetto.getStato()).isEqualTo(DEFAULT_STATO);
        assertThat(testProgetto.isAttivo()).isEqualTo(DEFAULT_ATTIVO);
    }

    @Test
    @Transactional
    public void createProgettoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = progettoRepository.findAll().size();

        // Create the Progetto with an existing ID
        progetto.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgettoMockMvc.perform(post("/api/progettos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(progetto)))
            .andExpect(status().isBadRequest());

        // Validate the Progetto in the database
        List<Progetto> progettoList = progettoRepository.findAll();
        assertThat(progettoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProgettos() throws Exception {
        // Initialize the database
        progettoRepository.saveAndFlush(progetto);

        // Get all the progettoList
        restProgettoMockMvc.perform(get("/api/progettos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(progetto.getId().intValue())))
            .andExpect(jsonPath("$.[*].titolo").value(hasItem(DEFAULT_TITOLO)))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE)))
            .andExpect(jsonPath("$.[*].importo").value(hasItem(DEFAULT_IMPORTO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
            .andExpect(jsonPath("$.[*].attivo").value(hasItem(DEFAULT_ATTIVO.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getProgetto() throws Exception {
        // Initialize the database
        progettoRepository.saveAndFlush(progetto);

        // Get the progetto
        restProgettoMockMvc.perform(get("/api/progettos/{id}", progetto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(progetto.getId().intValue()))
            .andExpect(jsonPath("$.titolo").value(DEFAULT_TITOLO))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE))
            .andExpect(jsonPath("$.importo").value(DEFAULT_IMPORTO.doubleValue()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()))
            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
            .andExpect(jsonPath("$.attivo").value(DEFAULT_ATTIVO.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingProgetto() throws Exception {
        // Get the progetto
        restProgettoMockMvc.perform(get("/api/progettos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgetto() throws Exception {
        // Initialize the database
        progettoService.save(progetto);

        int databaseSizeBeforeUpdate = progettoRepository.findAll().size();

        // Update the progetto
        Progetto updatedProgetto = progettoRepository.findById(progetto.getId()).get();
        // Disconnect from session so that the updates on updatedProgetto are not directly saved in db
        em.detach(updatedProgetto);
        updatedProgetto
            .titolo(UPDATED_TITOLO)
            .descrizione(UPDATED_DESCRIZIONE)
            .importo(UPDATED_IMPORTO)
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .stato(UPDATED_STATO)
            .attivo(UPDATED_ATTIVO);

        restProgettoMockMvc.perform(put("/api/progettos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProgetto)))
            .andExpect(status().isOk());

        // Validate the Progetto in the database
        List<Progetto> progettoList = progettoRepository.findAll();
        assertThat(progettoList).hasSize(databaseSizeBeforeUpdate);
        Progetto testProgetto = progettoList.get(progettoList.size() - 1);
        assertThat(testProgetto.getTitolo()).isEqualTo(UPDATED_TITOLO);
        assertThat(testProgetto.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testProgetto.getImporto()).isEqualTo(UPDATED_IMPORTO);
        assertThat(testProgetto.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
        assertThat(testProgetto.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
        assertThat(testProgetto.getStato()).isEqualTo(UPDATED_STATO);
        assertThat(testProgetto.isAttivo()).isEqualTo(UPDATED_ATTIVO);
    }

    @Test
    @Transactional
    public void updateNonExistingProgetto() throws Exception {
        int databaseSizeBeforeUpdate = progettoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgettoMockMvc.perform(put("/api/progettos").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(progetto)))
            .andExpect(status().isBadRequest());

        // Validate the Progetto in the database
        List<Progetto> progettoList = progettoRepository.findAll();
        assertThat(progettoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgetto() throws Exception {
        // Initialize the database
        progettoService.save(progetto);

        int databaseSizeBeforeDelete = progettoRepository.findAll().size();

        // Delete the progetto
        restProgettoMockMvc.perform(delete("/api/progettos/{id}", progetto.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Progetto> progettoList = progettoRepository.findAll();
        assertThat(progettoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
