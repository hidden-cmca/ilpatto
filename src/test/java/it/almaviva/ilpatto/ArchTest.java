package it.almaviva.ilpatto;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("it.almaviva.ilpatto");

        noClasses()
            .that()
                .resideInAnyPackage("it.almaviva.ilpatto.service..")
            .or()
                .resideInAnyPackage("it.almaviva.ilpatto.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..it.almaviva.ilpatto.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
