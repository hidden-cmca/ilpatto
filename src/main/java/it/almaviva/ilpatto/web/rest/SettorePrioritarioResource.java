package it.almaviva.ilpatto.web.rest;

import it.almaviva.ilpatto.domain.SettorePrioritario;
import it.almaviva.ilpatto.service.SettorePrioritarioService;
import it.almaviva.ilpatto.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link it.almaviva.ilpatto.domain.SettorePrioritario}.
 */
@RestController
@RequestMapping("/api")
public class SettorePrioritarioResource {

    private final Logger log = LoggerFactory.getLogger(SettorePrioritarioResource.class);

    private static final String ENTITY_NAME = "ilpattoSettorePrioritario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SettorePrioritarioService settorePrioritarioService;

    public SettorePrioritarioResource(SettorePrioritarioService settorePrioritarioService) {
        this.settorePrioritarioService = settorePrioritarioService;
    }

    /**
     * {@code POST  /settore-prioritarios} : Create a new settorePrioritario.
     *
     * @param settorePrioritario the settorePrioritario to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new settorePrioritario, or with status {@code 400 (Bad Request)} if the settorePrioritario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/settore-prioritarios")
    public ResponseEntity<SettorePrioritario> createSettorePrioritario(@RequestBody SettorePrioritario settorePrioritario) throws URISyntaxException {
        log.debug("REST request to save SettorePrioritario : {}", settorePrioritario);
        if (settorePrioritario.getId() != null) {
            throw new BadRequestAlertException("A new settorePrioritario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SettorePrioritario result = settorePrioritarioService.save(settorePrioritario);
        return ResponseEntity.created(new URI("/api/settore-prioritarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /settore-prioritarios} : Updates an existing settorePrioritario.
     *
     * @param settorePrioritario the settorePrioritario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated settorePrioritario,
     * or with status {@code 400 (Bad Request)} if the settorePrioritario is not valid,
     * or with status {@code 500 (Internal Server Error)} if the settorePrioritario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/settore-prioritarios")
    public ResponseEntity<SettorePrioritario> updateSettorePrioritario(@RequestBody SettorePrioritario settorePrioritario) throws URISyntaxException {
        log.debug("REST request to update SettorePrioritario : {}", settorePrioritario);
        if (settorePrioritario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SettorePrioritario result = settorePrioritarioService.save(settorePrioritario);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, settorePrioritario.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /settore-prioritarios} : get all the settorePrioritarios.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of settorePrioritarios in body.
     */
    @GetMapping("/settore-prioritarios")
    public ResponseEntity<List<SettorePrioritario>> getAllSettorePrioritarios(Pageable pageable) {
        log.debug("REST request to get a page of SettorePrioritarios");
        Page<SettorePrioritario> page = settorePrioritarioService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /settore-prioritarios/:id} : get the "id" settorePrioritario.
     *
     * @param id the id of the settorePrioritario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the settorePrioritario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/settore-prioritarios/{id}")
    public ResponseEntity<SettorePrioritario> getSettorePrioritario(@PathVariable Long id) {
        log.debug("REST request to get SettorePrioritario : {}", id);
        Optional<SettorePrioritario> settorePrioritario = settorePrioritarioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(settorePrioritario);
    }

    /**
     * {@code DELETE  /settore-prioritarios/:id} : delete the "id" settorePrioritario.
     *
     * @param id the id of the settorePrioritario to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/settore-prioritarios/{id}")
    public ResponseEntity<Void> deleteSettorePrioritario(@PathVariable Long id) {
        log.debug("REST request to delete SettorePrioritario : {}", id);
        settorePrioritarioService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
