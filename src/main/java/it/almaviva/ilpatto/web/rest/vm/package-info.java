/**
 * View Models used by Spring MVC REST controllers.
 */
package it.almaviva.ilpatto.web.rest.vm;
