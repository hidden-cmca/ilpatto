package it.almaviva.ilpatto.web.rest;

import it.almaviva.ilpatto.domain.Progetto;
import it.almaviva.ilpatto.service.ProgettoService;
import it.almaviva.ilpatto.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link it.almaviva.ilpatto.domain.Progetto}.
 */
@RestController
@RequestMapping("/api")
public class ProgettoResource {

    private final Logger log = LoggerFactory.getLogger(ProgettoResource.class);

    private static final String ENTITY_NAME = "ilpattoProgetto";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgettoService progettoService;

    public ProgettoResource(ProgettoService progettoService) {
        this.progettoService = progettoService;
    }

    /**
     * {@code POST  /progettos} : Create a new progetto.
     *
     * @param progetto the progetto to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new progetto, or with status {@code 400 (Bad Request)} if the progetto has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/progettos")
    public ResponseEntity<Progetto> createProgetto(@RequestBody Progetto progetto) throws URISyntaxException {
        log.debug("REST request to save Progetto : {}", progetto);
        if (progetto.getId() != null) {
            throw new BadRequestAlertException("A new progetto cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Progetto result = progettoService.save(progetto);
        return ResponseEntity.created(new URI("/api/progettos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /progettos} : Updates an existing progetto.
     *
     * @param progetto the progetto to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated progetto,
     * or with status {@code 400 (Bad Request)} if the progetto is not valid,
     * or with status {@code 500 (Internal Server Error)} if the progetto couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/progettos")
    public ResponseEntity<Progetto> updateProgetto(@RequestBody Progetto progetto) throws URISyntaxException {
        log.debug("REST request to update Progetto : {}", progetto);
        if (progetto.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Progetto result = progettoService.save(progetto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, progetto.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /progettos} : get all the progettos.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of progettos in body.
     */
    @GetMapping("/progettos")
    public ResponseEntity<List<Progetto>> getAllProgettos(Pageable pageable) {
        log.debug("REST request to get a page of Progettos");
        Page<Progetto> page = progettoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /progettos/:id} : get the "id" progetto.
     *
     * @param id the id of the progetto to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the progetto, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/progettos/{id}")
    public ResponseEntity<Progetto> getProgetto(@PathVariable Long id) {
        log.debug("REST request to get Progetto : {}", id);
        Optional<Progetto> progetto = progettoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(progetto);
    }

    /**
     * {@code DELETE  /progettos/:id} : delete the "id" progetto.
     *
     * @param id the id of the progetto to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/progettos/{id}")
    public ResponseEntity<Void> deleteProgetto(@PathVariable Long id) {
        log.debug("REST request to delete Progetto : {}", id);
        progettoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
