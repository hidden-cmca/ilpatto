package it.almaviva.ilpatto.domain.enumeration;

/**
 * The Stati enumeration.
 */
public enum Stati {
    PROGETTAZIONE, PROGRAMMAZIONE, ESECUZIONE
}
