package it.almaviva.ilpatto.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

import it.almaviva.ilpatto.domain.enumeration.Stati;

/**
 * A Progetto.
 */
@Entity
@Table(name = "progetto")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Progetto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "titolo")
    private String titolo;

    @Column(name = "descrizione")
    private String descrizione;

    @Column(name = "importo")
    private Double importo;

    @Column(name = "data_inizio")
    private LocalDate dataInizio;

    @Column(name = "data_fine")
    private LocalDate dataFine;

    @Enumerated(EnumType.STRING)
    @Column(name = "stato")
    private Stati stato;

    @Column(name = "attivo")
    private Boolean attivo;

    @ManyToOne
    @JsonIgnoreProperties(value = "progettos", allowSetters = true)
    private Comune comune;

    @ManyToOne
    @JsonIgnoreProperties(value = "progettos", allowSetters = true)
    private Area area;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitolo() {
        return titolo;
    }

    public Progetto titolo(String titolo) {
        this.titolo = titolo;
        return this;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Progetto descrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Double getImporto() {
        return importo;
    }

    public Progetto importo(Double importo) {
        this.importo = importo;
        return this;
    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }

    public LocalDate getDataInizio() {
        return dataInizio;
    }

    public Progetto dataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
        return this;
    }

    public void setDataInizio(LocalDate dataInizio) {
        this.dataInizio = dataInizio;
    }

    public LocalDate getDataFine() {
        return dataFine;
    }

    public Progetto dataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
        return this;
    }

    public void setDataFine(LocalDate dataFine) {
        this.dataFine = dataFine;
    }

    public Stati getStato() {
        return stato;
    }

    public Progetto stato(Stati stato) {
        this.stato = stato;
        return this;
    }

    public void setStato(Stati stato) {
        this.stato = stato;
    }

    public Boolean isAttivo() {
        return attivo;
    }

    public Progetto attivo(Boolean attivo) {
        this.attivo = attivo;
        return this;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public Comune getComune() {
        return comune;
    }

    public Progetto comune(Comune comune) {
        this.comune = comune;
        return this;
    }

    public void setComune(Comune comune) {
        this.comune = comune;
    }

    public Area getArea() {
        return area;
    }

    public Progetto area(Area area) {
        this.area = area;
        return this;
    }

    public void setArea(Area area) {
        this.area = area;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Progetto)) {
            return false;
        }
        return id != null && id.equals(((Progetto) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Progetto{" +
            "id=" + getId() +
            ", titolo='" + getTitolo() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", importo=" + getImporto() +
            ", dataInizio='" + getDataInizio() + "'" +
            ", dataFine='" + getDataFine() + "'" +
            ", stato='" + getStato() + "'" +
            ", attivo='" + isAttivo() + "'" +
            "}";
    }
}
