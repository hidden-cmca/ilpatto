package it.almaviva.ilpatto.repository;

import it.almaviva.ilpatto.domain.Progetto;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Progetto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgettoRepository extends JpaRepository<Progetto, Long> {
}
