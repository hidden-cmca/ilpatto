package it.almaviva.ilpatto.repository;

import it.almaviva.ilpatto.domain.SettorePrioritario;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SettorePrioritario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SettorePrioritarioRepository extends JpaRepository<SettorePrioritario, Long> {
}
