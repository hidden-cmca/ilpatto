package it.almaviva.ilpatto.service;

import it.almaviva.ilpatto.domain.Progetto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Progetto}.
 */
public interface ProgettoService {

    /**
     * Save a progetto.
     *
     * @param progetto the entity to save.
     * @return the persisted entity.
     */
    Progetto save(Progetto progetto);

    /**
     * Get all the progettos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Progetto> findAll(Pageable pageable);


    /**
     * Get the "id" progetto.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Progetto> findOne(Long id);

    /**
     * Delete the "id" progetto.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
