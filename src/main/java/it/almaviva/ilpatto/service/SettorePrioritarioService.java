package it.almaviva.ilpatto.service;

import it.almaviva.ilpatto.domain.SettorePrioritario;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link SettorePrioritario}.
 */
public interface SettorePrioritarioService {

    /**
     * Save a settorePrioritario.
     *
     * @param settorePrioritario the entity to save.
     * @return the persisted entity.
     */
    SettorePrioritario save(SettorePrioritario settorePrioritario);

    /**
     * Get all the settorePrioritarios.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SettorePrioritario> findAll(Pageable pageable);


    /**
     * Get the "id" settorePrioritario.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SettorePrioritario> findOne(Long id);

    /**
     * Delete the "id" settorePrioritario.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
