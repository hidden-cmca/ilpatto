package it.almaviva.ilpatto.service.impl;

import it.almaviva.ilpatto.service.SettorePrioritarioService;
import it.almaviva.ilpatto.domain.SettorePrioritario;
import it.almaviva.ilpatto.repository.SettorePrioritarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SettorePrioritario}.
 */
@Service
@Transactional
public class SettorePrioritarioServiceImpl implements SettorePrioritarioService {

    private final Logger log = LoggerFactory.getLogger(SettorePrioritarioServiceImpl.class);

    private final SettorePrioritarioRepository settorePrioritarioRepository;

    public SettorePrioritarioServiceImpl(SettorePrioritarioRepository settorePrioritarioRepository) {
        this.settorePrioritarioRepository = settorePrioritarioRepository;
    }

    @Override
    public SettorePrioritario save(SettorePrioritario settorePrioritario) {
        log.debug("Request to save SettorePrioritario : {}", settorePrioritario);
        return settorePrioritarioRepository.save(settorePrioritario);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<SettorePrioritario> findAll(Pageable pageable) {
        log.debug("Request to get all SettorePrioritarios");
        return settorePrioritarioRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<SettorePrioritario> findOne(Long id) {
        log.debug("Request to get SettorePrioritario : {}", id);
        return settorePrioritarioRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SettorePrioritario : {}", id);
        settorePrioritarioRepository.deleteById(id);
    }
}
