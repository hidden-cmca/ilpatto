package it.almaviva.ilpatto.service.impl;

import it.almaviva.ilpatto.service.ComuneService;
import it.almaviva.ilpatto.domain.Comune;
import it.almaviva.ilpatto.repository.ComuneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Comune}.
 */
@Service
@Transactional
public class ComuneServiceImpl implements ComuneService {

    private final Logger log = LoggerFactory.getLogger(ComuneServiceImpl.class);

    private final ComuneRepository comuneRepository;

    public ComuneServiceImpl(ComuneRepository comuneRepository) {
        this.comuneRepository = comuneRepository;
    }

    @Override
    public Comune save(Comune comune) {
        log.debug("Request to save Comune : {}", comune);
        return comuneRepository.save(comune);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Comune> findAll(Pageable pageable) {
        log.debug("Request to get all Comunes");
        return comuneRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Comune> findOne(Long id) {
        log.debug("Request to get Comune : {}", id);
        return comuneRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Comune : {}", id);
        comuneRepository.deleteById(id);
    }
}
