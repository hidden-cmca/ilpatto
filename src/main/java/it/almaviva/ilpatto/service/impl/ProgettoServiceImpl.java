package it.almaviva.ilpatto.service.impl;

import it.almaviva.ilpatto.service.ProgettoService;
import it.almaviva.ilpatto.domain.Progetto;
import it.almaviva.ilpatto.repository.ProgettoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Progetto}.
 */
@Service
@Transactional
public class ProgettoServiceImpl implements ProgettoService {

    private final Logger log = LoggerFactory.getLogger(ProgettoServiceImpl.class);

    private final ProgettoRepository progettoRepository;

    public ProgettoServiceImpl(ProgettoRepository progettoRepository) {
        this.progettoRepository = progettoRepository;
    }

    @Override
    public Progetto save(Progetto progetto) {
        log.debug("Request to save Progetto : {}", progetto);
        return progettoRepository.save(progetto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Progetto> findAll(Pageable pageable) {
        log.debug("Request to get all Progettos");
        return progettoRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Progetto> findOne(Long id) {
        log.debug("Request to get Progetto : {}", id);
        return progettoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Progetto : {}", id);
        progettoRepository.deleteById(id);
    }
}
